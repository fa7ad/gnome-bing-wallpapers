#!/bin/bash
# Bing wallpaper script.
# Originally posted and developed in ubuntuforums by others
# Modified later by me <@devilv15 || evilinfinite7@gmail.com>
bing="www.bing.com"
idx=$( shuf -i 0-7 -n 1 )
xmlURL="http://www.bing.com/HPImageArchive.aspx?format=xml&idx=$idx&n=1&mkt=en-US"
saveDir=$HOME'/.cache/bing/'
echo Preparing Folders...
mkdir -p $saveDir
picOpts="zoom"
# Valid options: "_1024x768" "_1280x720" "_1366x768" "_1920x1200" "_1920x1080"
picRes="_1920x1080"
picExt=".jpg"
echo Getting the URL of the photo...
picURL=$bing$(echo $(curl -s $xmlURL) | grep -oP "<urlBase>(.*)</urlBase>" | cut -d ">" -f 2 | cut -d "<" -f 1)$picRes$picExt
tempName="temp.jpg"
picName=${picURL##*/}
if [ ! -f "$saveDir$picName" ]
    then
    echo Cached file does not exist!
    echo Downloading Photo...
    curl -s -o $saveDir$tempName $picURL
    annotation=$(echo $(curl -s $xmlURL) | grep -oP "<copyright>(.*)</copyright>" | rev | cut -d "(" -f 1 | rev | cut -d ")" -f 1)
    echo Adding Copyright info...
    convert $saveDir$tempName  -fill white  -undercolor '#00000080' -font "Sans" -pointsize 18 -gravity Southeast -annotate +0+5 " $annotation " $saveDir$picName
    echo Removing Temporary files
    rm $saveDir$tempName
fi
echo Opening Preview...
shotwell $saveDir$picName
echo Setting the photo as Lock-screen Background
DISPLAY=:0 GSETTINGS_BACKEND=dconf gsettings set org.gnome.desktop.screensaver picture-uri '"file://'$saveDir$picName'"'
DISPLAY=:0 GSETTINGS_BACKEND=dconf gsettings set org.gnome.desktop.screensaver picture-options $picOpts
echo Removing old cache...
find $saveDir -atime 7 -delete
echo Finished!
exit
