GNOME-Bing-Wallpapers
=====================

A script that grabs Bing's picture of the day from the last 7 days randomly and sets it as the Desktop and Lockscreen Background

How to Install
==============
* Grab this repo (clone it or download as zip from github)
* Execute `install.sh` from a terminal window
* Enter your password to grant permissions

How to use it :D
================
* Press **ALT-F2** or open a **Terminal** and run `bing-wallpaper` or `bing-lockscreen`
            to change the _Wallpaper_ or _Lockscreen_
* (optional) Make a cron task that executes these scripts every hour/5 mins
* step.next == null; enjoy("xD");
