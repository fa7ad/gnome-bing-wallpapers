GNOME-Bing-Wallpapers
=====================

A bash program that grabs Bing's picture of the day from the last 7 days randomly and sets it as the Desktop and/or Lockscreen Background

How to Install
==============
### Ubuntu (14.04 or later)
* Enable my repo
  * Just execute this in a terminal  
  ```curl https://packagecloud.io/install/repositories/fa7ad/bing-bg/script.deb.sh | sudo bash```  
* And **install** the package
  * ```sudo apt-get install bing-backgrounds```

### Fedora (21 or later)
* Enable my repo
  * Just execute this in a terminal  
  ```curl https://packagecloud.io/install/repositories/fa7ad/bing-bg/script.rpm.sh | sudo bash```  
* And **install** the package
  * ```sudo yum install bing-backgrounds```

How to use it :D
================
* Open **Bing Backgrounds** from the menu/dash  

 **OR**  

* Press **ALT-F2** or open a **Terminal** and run `bingbg`
* step.next == null; enjoy("xD");

Original idea by a user on ubuntu forums