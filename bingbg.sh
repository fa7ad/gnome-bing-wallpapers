#!/bin/bash
function run {
    saveDir="$HOME/.cache/bingbg/"
    idx=$( shuf -i 0-7 -n 1 )
    xmlURL="http://www.bing.com/HPImageArchive.aspx?format=xml&idx=$idx&n=1&mkt=en-US"
    cd $HOME
    mkdir -p $saveDir
    picURL="http://www.bing.com/"$(curl -Ls $xmlURL | tee >(zenity --progress --title "Bing BG" --pulsate --text "Getting the URL" --auto-close) | grep -oP "<urlBase>(.*)</urlBase>" | cut -d ">" -f 2 | cut -d "<" -f 1)"_1920x1080.jpg"
    tempName="temp.jpg"
    picName=${picURL##*/}
    if [ ! -f "$saveDir$picName" ];then
        curl -sL -o $saveDir$tempName $picURL | tee >(zenity --progress --title "Bing BG" --pulsate --text "Downloading the photo..." --auto-close)
        annotation=$(curl -sL $xmlURL | grep -oP "<copyright>(.*)</copyright>" | rev | cut -d "(" -f 1 | rev | cut -d ")" -f 1)
        convert $saveDir$tempName  -fill white  -undercolor '#00000080' -font "Sans" -pointsize 18 -gravity Southeast -annotate +0+5 " $annotation " $saveDir$picName
        rm $saveDir$tempName
    fi
    (echo "<img width=\"250px\" src=\"data:"
    mimetype -b "$saveDir$picName"
    echo -n ";base64,"
    base64 "$saveDir$picName"
    echo "\">"
    echo '<h3>Press OK if you like the image</h3>'
    echo '<p>Press cancel and run the program again if you dont :) </p>') | zenity --text-info --title "Preview" --html --filename=/dev/stdin
    preview=$?
    if [ "$preview" != "1" ];then
        ans=$(zenity --title "Bing BG"  --list  --text "What do you want to change?" --checklist --column "Select" --column "Option" TRUE Wallpaper FALSE Lockscreen --separator "+")
        case $ans in
        Wallpaper)
            DISPLAY=:0 GSETTINGS_BACKEND=dconf gsettings set org.gnome.desktop.background picture-uri '"file://'$saveDir$picName'"'
            DISPLAY=:0 GSETTINGS_BACKEND=dconf gsettings set org.gnome.desktop.background picture-options "zoom"
            ;;
        Lockscreen)
            DISPLAY=:0 GSETTINGS_BACKEND=dconf gsettings set org.gnome.desktop.screensaver picture-uri '"file://'$saveDir$picName'"'
            DISPLAY=:0 GSETTINGS_BACKEND=dconf gsettings set org.gnome.desktop.screensaver picture-options "zoom"
            ;;
        *)
            DISPLAY=:0 GSETTINGS_BACKEND=dconf gsettings set org.gnome.desktop.background picture-uri '"file://'$saveDir$picName'"'
            DISPLAY=:0 GSETTINGS_BACKEND=dconf gsettings set org.gnome.desktop.background picture-options "zoom"
            DISPLAY=:0 GSETTINGS_BACKEND=dconf gsettings set org.gnome.desktop.screensaver picture-uri '"file://'$saveDir$picName'"'
            DISPLAY=:0 GSETTINGS_BACKEND=dconf gsettings set org.gnome.desktop.screensaver picture-options "zoom"
            ;;
        esac
    fi
}

zenity --window-icon="info" --question --title "Bing BG" --text 'This will download a picture from bing.\n\nDo you want to continue?'
welcome=$?
if [ "$welcome" = "0" ];then
    run
fi
